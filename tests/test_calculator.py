import requests
import random
import endpoints


def test_add_calculator():
    a = random.randint(1,10)
    b = random.randint(1,10)
    body = {
        "firstNumber": a,
        "secondNumber": b
    }
    resp = requests.post(endpoints.calculator_add, json=body)  # jakie ma byc body
    assert resp.status_code == 200     #sprawdzamy czy to dodawanie daje ok

    actual_res = resp.json()["result"]
    exp_result = a + b
    assert actual_res == exp_result

