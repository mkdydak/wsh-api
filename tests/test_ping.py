import requests
import endpoints


def test_ping():
    response = requests.get(endpoints.ping)
    assert response.status_code == 200
    assert response.text == 'pong'

def test_ping_as_json():
    headers_dict = {
        'Accept' : 'application/json'
    }
    ping_response = requests.get(endpoints.ping, headers=headers_dict)
    assert ping_response.status_code == 200
    response_dict = ping_response.json()
    assert response_dict['reply'] == 'pong!'


