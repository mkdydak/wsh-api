import pytest
import requests
from faker import Faker            #z biblioteki faker import klasa Faker
import endpoints
from models.account import Account

fake = Faker()                         #inicjujemy obiekt klasy Faker

# test sprawdza czy na liscie jest imie
# response = requests.get(endpoints.accounts)   #potrafimy zrobic zapytanie get
# assert response.status_code == 200                 #jezeli to nie bedzie 200 to test powinien się wywalić
# response_dict = response.json()                 #potrafimy stworzyc slownik z odpowiedzi get
#  print(response_dict['accounts'])

def test_get_accounts_list(new_account):
    response = requests.get(endpoints.accounts)
    assert response.status_code == 200

    response_dict = response.json()

    accounts_list = response_dict['accounts']
    names_list = [a['name'] for a in accounts_list]
    print(names_list)
    assert new_account in names_list

# def test_create_account():    #stworzenie nowego uzytkowniaka, sprawdzamy czy imie jest na liscie #slownik zamieni na postac jsona (pod
# geta z paramsami

def test_create_account(new_account):
    list_params = {'account': new_account}
    filtered_list_response = requests.get(endpoints.accounts, params=list_params)
    assert filtered_list_response.status_code == 200
    assert new_account in filtered_list_response.text

# usunięcie konta

def test_delete_account(new_account):
    new_account.delete()

    account_params = {'account': new_account.name}
    filtered_list_response = requests.get(endpoints.accounts, params=account_params)
    assert filtered_list_response.status_code == 404

# ta _create_account funkcja udostepnia nam dane testowe, sa one indywidualne

# sprawdzamy czy na koncie użytkownika jest 1000 PLN
# korzystamy z debugera

def test_check_ammount_balance(new_account):
    assert new_account.get_balance() == 1000



# klucz do accounts, a na pozycji 0 jest slownik, ktory ma 2 klucze, name to string i balance - ma obiekt slownik
# indeksowanie listy jest intami, a kluczy slownikami

def test_case(new_account):
    new_account.pay(200)
    assert new_account.get_balance() == 1200
    new_account.withdraw(133)
    assert new_account.get_balance() == 1067

@pytest.fixture
def new_account():
    account = Account()
    account.create()
    return account

#zwraca nowy obiekt na którym bedziemy mogli operować






