from faker import Faker
import requests
import endpoints

fake = Faker()


class Account:
    def __init__(self):
        self.name = fake.uuid4()     # kazdy obiekt ma nowy name
        self.balance = None

    def create(self):
        body = {"name": self.name}
        create_account_response = requests.put(endpoints.accounts_create, json=body)    #import request bo świeci się na czerwowo
        assert create_account_response.status_code == 201

    def delete(self):
        account_name_params = {'account': self.name}
        response_delete_account = requests.delete(endpoints.accounts_delete, params=account_name_params)
        assert response_delete_account.status_code == 200

    def pay(self, amount):
        account_params = {'account': self.name}
        body = {'amount': amount}
        pay_response = requests.post(endpoints.accounts_pay, json=body, params=account_params)
        assert pay_response.status_code == 200


    def withdraw(self, amount):
        account_params = {'account': self.name}
        body = {'amount': amount}
        pay_response = requests.post(endpoints.accounts_withdraw, json=body, params=account_params)
        assert pay_response.status_code == 200

    def get_balance(self):
        account_name_params = {'account': self.name}
        filtered_list_response = requests.get(endpoints.accounts, params=account_name_params)
        return filtered_list_response.json()['accounts'][0]['balance']['accountBalance']


if __name__ == '__main__':       #ctr j main
    account = Account()
    print(account.name)
    account.create()
    account.delete()